import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DataService} from './services/data.service'
import { Http} from '@angular/http';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { AppRouteModule } from './modules/app-route/app-route.module';
import {NgxBootstrapModulesModule} from './modules/ngx-bootstrap-modules/ngx-bootstrap-modules.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgxBootstrapModulesModule,
    CollapseModule.forRoot(),
    AppRouteModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
