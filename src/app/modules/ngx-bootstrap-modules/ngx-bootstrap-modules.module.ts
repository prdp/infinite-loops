import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

@NgModule({
  imports: [
    CommonModule,
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
  ],
  exports: [
    AlertModule,
    ButtonsModule
  ],
  declarations: []
})
export class NgxBootstrapModulesModule { }
