import { NgxBootstrapModulesModule } from './ngx-bootstrap-modules.module';

describe('NgxBootstrapModulesModule', () => {
  let ngxBootstrapModulesModule: NgxBootstrapModulesModule;

  beforeEach(() => {
    ngxBootstrapModulesModule = new NgxBootstrapModulesModule();
  });

  it('should create an instance', () => {
    expect(ngxBootstrapModulesModule).toBeTruthy();
  });
});
