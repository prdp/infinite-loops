import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ListingComponent } from '../../pages/listing/listing.component';
import { ListdetailsComponent } from '../../pages/listdetails/listdetails.component';


const appRoutes: Routes = [
  { path: '', component: ListingComponent },
  { path: 'details',        component: ListdetailsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [ListingComponent, ListdetailsComponent]
})
export class AppRouteModule { }
