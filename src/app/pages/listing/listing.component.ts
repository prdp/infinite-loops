import { Component, OnInit } from '@angular/core';
import { Router }                 from '@angular/router';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  allData:any;
  isCollapsed = true;
  constructor(
    private router: Router,
    private data:DataService) { 
  }

  ngOnInit() {
    this.allData=[{"id":1,"product_code":"WN","name":"WINE","quantity":"500","supplier_id":"10","price":"5000.00","created_at":null,"updated_at":null,"deleted_at":null},{"id":2,"product_code":"BR","name":"BEER","quantity":"100","supplier_id":"11","price":"100.00","created_at":null,"updated_at":null,"deleted_at":null},{"id":3,"product_code":"VDK","name":"VODKA","quantity":"200","supplier_id":"12","price":"200.00","created_at":null,"updated_at":null,"deleted_at":null},{"id":4,"product_code":"SC","name":"SCOTCH","quantity":"400","supplier_id":"12","price":"400.00","created_at":null,"updated_at":null,"deleted_at":null},{"id":5,"product_code":"WS","name":"WHISKY","quantity":"300","supplier_id":"10","price":"300.00","created_at":null,"updated_at":null,"deleted_at":null}]
    //this.allData=this.data.getListing();
    // this.data.getListing().subscribe(
    //   res => {
    //     console.log(res)
    //     this.allData=res;
    //   },
    //   error => console.log("error: " + error)
    // );
  }
  
  viewDetails(data) {
    this.router.navigate(['details', data]);
  }
}
