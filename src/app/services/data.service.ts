import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptions } from "@angular/http";




@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiUrl: string='http://192.168.1.245/products/1';
  options:any;
  constructor(private http: Http) { 
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: headers});

  }

  getListing() {
    let obs = new Observable(observer => {
      this.http.get(this.apiUrl,this.options).subscribe(
          (response: Response) => {
              observer.next(response.json());
              observer.complete();
          },
          error=> {
              observer.error(error);
          });
  });
  return obs;
//   let obs = new Observable(observer => {
//     this.http.post(this.apiUrl,this.options).subscribe(
//         (response: Response) => {
//             observer.next(response.json());
//             observer.complete();
//         },
//         error=> {
//             observer.error(error);
//         });
// });
// return obs;


  }
}
